resource "azurerm_resource_group" "ktstream" {
  name     = var.rgname
  location = var.rglocation
}

variable "rgname" {
  type = string
}

variable "rglocation" {
  type = string
  default = "centralus"
}
